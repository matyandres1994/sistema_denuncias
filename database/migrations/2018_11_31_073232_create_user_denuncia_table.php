<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDenunciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_denuncia', function (Blueprint $table) {
            $table->increments('id');
            //$table->date('fecha_inicio');
            //$table->date('fecha_fin');

            $table->integer('user_id')->unsigned();
            $table->integer('denuncia_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('denuncia_id')->references('id')->on('denuncias')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_denuncia', function (Blueprint $table) {
            //Schema::drop('user_rol');
        });
    }
}
