<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            //Migrar de nuevo este
            $table->string('tipo_estado');
            //voy a cambiar el dejar un estado por defecto a un Create de de estados
            //$table->enum('tipo_estado',['solicitado','aceptado','rechazado','proceso','baja'])->default('solicitado');
            //ya lo hice..
 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estados');
    }
}
