<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('denuncias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_denuncia');
            $table->date('fecha_agresion');
            $table->string('pregunta1');
            $table->string('pregunta2');
            $table->string('pregunta3');
            $table->string('subir_archivo');
            
            //agregar 'no solicitar ayuda', cambiar asesor_legal
            $table->enum('tipo_ayuda', ['psicologico','medico','asesor legal','no solicitar'])->default('no solicitar');

            //claves foraneas
            $table->integer('id_tipo_denuncia')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_user')->unsigned();
            //agregada 2 de enero
            //$table->integer('id_archivo_denuncia')->unsigned();

            $table->foreign('id_tipo_denuncia')->references('id')->on('tipos_denuncias')
                ->onDelete('cascade');
                //->onUpdate('cascade');

            $table->foreign('id_estado')->references('id')->on('estados')
                ->onDelete('cascade');
                //->onUpdate('cascade');

            $table->foreign('id_user')->references('id')->on('users')
                ->onDelete('cascade');
                //->onUpdate('cascade');
            //agregada 2 de enero    
            //$table->foreign('id_archivo_denuncia')->references('id')->on('archivos_denuncias')
            //    ->onDelete('cascade')
            //    ->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('denuncias');
    }
}
