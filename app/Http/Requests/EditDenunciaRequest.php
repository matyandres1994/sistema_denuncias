<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditDenunciaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_denuncia' => 'min:4|max:50|required',
            'fecha_agresion' => 'required',
            'pregunta1' => 'min:2|max:150|required',
            'pregunta2' => 'min:2|max:150|required',
            'pregunta3' => 'min:2|max:150|required',
            //'subir_archivo' => 'required',
            'tipo_ayuda' => 'required',
            'id_tipo_denuncia' => 'required',
            'id_estado' => 'required'
        ];
    }
}
