<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Freshwork\ChileanBundle\Rut;
use ValidateRequests;
use Malahierba\ChileRut\ChileRut;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'rut' => 'required|cl_rut|unique:users',
            'name' => 'required|max:15',
            'apellido' => 'required|min:2',
            'email' => 'required|email|max:255|unique:users',
            'telefono' => 'required|min:6|max:15|',
            'password' => 'required|confirmed|min:6',
            //'tipo_usuario' => 'required',
        ];
    }
}
