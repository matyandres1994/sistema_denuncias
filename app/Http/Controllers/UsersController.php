<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Rol;
use App\AsignarRol;
use Laracasts\Flash\Flash;
//use App\Http\Requests\UserRequest;

use Freshwork\ChileanBundle\Rut;
use ValidateRequests;
//use Validator;
//use Malahierba\ChileRut\ChileRut;

class UsersController extends Controller 
{
    

    public function index()
    {
    	$users = User::orderBy('id','ASC')->paginate(10);
        
        //$pastel = Pastel::sabor('chocolate')->first();
        //$users = User::select('189957726')->first();
        $users->each(function($users){
            
            $users->user_rol;
            
        });

        
            return view('admin.users.index')->with('users', $users);
        
        

            
        

        //dd("usuarioo");
    }

    public function create()
    {
        $users = User::orderBy('name','ASC')->lists('name','name');
    	return view('admin.users.createUser');
    }

    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'rut' => 'required|min:6|max:12|unique:users',
            'name' => 'required|max:15',
            'apellido' => 'required|min:2',
            'email' => 'required|email|max:255|unique:users',
            'telefono' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
        ]);
    }*/

    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'rut' => 'required|cl_rut',
            'name' => 'required|max:255',
            'apellido' => 'required|min:2',
            'email' => 'required|email|max:255|unique:users',
            'telefono' => 'required|min:6|max:15|',
            'password' => 'required|confirmed|min:6'
        ]);
        //-------metodo 1 de actualizado----
        /*$user = User::find($id);
        $user->fill($request->all());
        $user->save();
        return redirect()->route('admin.users.index');*/
        //-------metodo 1 de guardado-----
    	$user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        //-------metodo 2 de guardado------
        /*$user = User::find($id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->email = $request->email;
        $user->telefono = $request->telefono;
        //$user->tipo_usuario = $request->tipo_usuario;
        $user->save();*/
        //Flash::success("se ha registrado" . $user->name . "de forma exitosa!");
        return redirect()->route('admin.users.index');
    }

    public function show($id)
    {
    	//
    }

    public function edit($id)
    {
    	$user = User::find($id);
        return view('admin.users.editUser')->with('user', $user);

    }

    public function update(Request $request, $id)
    {
    	//dd($id);
        //-------metodo 1 de guardado----
        /*$user = User::find($id);
        $user->fill($request->all());
        $user->save();
        return redirect()->route('admin.users.index');*/

        //-------metodo 2 de guardado----
        $user = User::find($id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->email = $request->email;
        $user->telefono = $request->telefono;
        //$user->tipo_usuario = $request->tipo_usuario;
        $user->save();
        Flash::success("se ha registrado" . $user->name . "de forma exitosa!");
        return redirect()->route('admin.users.index');

    }

    public function destroy($id)
    {
    	$user = User::find($id);
        $user->delete();
        //Flash::warning('El usuario'. $user->name . 'ha sido borrado de forma exitosa!');
        return redirect()->route('admin.users.index');
    }


}
