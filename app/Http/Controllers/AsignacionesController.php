<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Denuncia;
use App\User;
use App\Asignar;
use App\Estado;
use App\Http\Requests\AsignacionRequest;


class AsignacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_denuncia = Asignar::orderBy('id','ASC')->paginate(13);
        

        //dd($user_denuncia);

        return view('admin.user_denuncia.index')->with('user_denuncia',$user_denuncia);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd('funcaa');
        $users = User::orderBy('name','ASC')->lists('name','id');
        $denuncias = Denuncia::orderBy('nombre_denuncia','ASC')->lists('nombre_denuncia','id');

        $user_denuncia = Asignar::all();

        return view('admin.user_denuncia.createAsignacion')
            ->with('users',$users)
            ->with('denuncias',$denuncias);
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsignacionRequest $request)
    {
        //dd($request->all());

        $user_denuncia = new Asignar($request->all());
        $user_denuncia->save();

        return redirect()->route('admin.user_denuncia.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
