<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Denuncia;
use App\User;
use App\Estado;
use App\Tipo_denuncia;
use App\Asignar;
use App\AsignarRol;
use Carbon\Carbon;

use App\Http\Requests\DenunciaRequest;
use App\Http\Requests\EditDenunciaRequest;


class DenunciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){

        Carbon::setLocale('es');
    }

    

    public function index()
    {
        //dd('hola');
        $denuncias = Denuncia::orderBy('id','ASC')->paginate(3);
        $denuncias->each(function($denuncias){
            
            $denuncias->tipos_denuncias;
            $denuncias->estados;
            
        });
        //dd($denuncias);
        return view('admin.denuncias.index')->with('denuncias',$denuncias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = Estado::orderBy('tipo_estado','ASC')->lists('tipo_estado','id');
        $tipos_denuncias = Tipo_denuncia::orderBy('tipo','ASC')->lists('tipo','id');
        $users = User::orderBy('name','ASC')->lists('name','id');

        $denuncias = Denuncia::all();

        return view('admin.denuncias.createDenuncia', ['denuncias' => $denuncias])
            ->with('estados',$estados)
            ->with('tipos_denuncias',$tipos_denuncias)
            ->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //dd($request->file('subir_archivo'));
        $denuncias = new Denuncia($request->all());
        $name      = $request->file('subir_archivo')->getClientOriginalName();
        $request->file('subir_archivo')->move('../uploads', $name);
        $denuncias->subir_archivo = $name;
        //mandar encriptado el nombre del archivo
        //$denuncias->subir_archivo = bcrypt($request->subir_archivo);

        $denuncias->save();
        

        return redirect()->route('admin.denuncias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $denuncia = Denuncia::find($id);
        $denuncia->tipo_denuncia;
        $denuncia->estado;
        $denuncia->user;

        $tipos_denuncias = Tipo_denuncia::orderBy('tipo','DESC')->lists('tipo','id');
        $estados = Estado::orderBy('tipo_estado','DESC')->lists('tipo_estado','id');
        $users = User::orderBy('rut','DESC')->lists('rut','id');

        return view('admin.denuncias.editDenuncia')
            ->with('denuncia',$denuncia)
            ->with('estados',$estados)
            ->with('tipos_denuncias',$tipos_denuncias)
            ->with('users',$users);
    }
    public function editEstadoDenuncia($id){
        //dd('hizo algo');

        $denuncia = Denuncia::find($id);
        $denuncia->estado;
        $estados = Estado::orderBy('tipo_estado','DESC')->lists('tipo_estado','id');

        return view('admin.denuncias.editEstadoDenuncia')
            ->with('denuncia',$denuncia)
            ->with('estados',$estados);

    }
    public function updateEstadoDenuncia(Request $request, $id){
        //dd('entro al update');
        $denuncia = Denuncia::find($id);
        $denuncia->id_estado = $request->id_estado;

        $denuncia->save();

        return redirect()->route('admin.denuncias.index');



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $denuncia = Denuncia::find($id);

        $denuncia->nombre_denuncia = $request->nombre_denuncia;
        $denuncia->fecha_agresion = $request->fecha_agresion;
        $denuncia->pregunta1 = $request->pregunta1;
        $denuncia->pregunta2 = $request->pregunta2;
        $denuncia->pregunta3 = $request->pregunta3;
        //$denuncia->subir_archivo = $request->subir_archivo;
        $denuncia->tipo_ayuda = $request->tipo_ayuda;
        $denuncia->id_tipo_denuncia = $request->id_tipo_denuncia;
        $denuncia->id_estado = $request->id_estado;
        //$denuncia->id_user = $request->id_user;


        $denuncia->save();

        return redirect()->route('admin.denuncias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $denuncias = Denuncia::find($id);
        $denuncias->delete();

        return redirect()->route('admin.denuncias.index');
    }

    /*public function mostrarDato(){
        //dd('kong');
        $allinfo = [];

        $f = [];

        $f = DB::select('select * from users');

        $allInfo = DB::select('select 
            d.id,
            d.nombre,
            td.id,
            td.nombre,
            from denuncias d 
            inner join tipo_denuncias td on td.id=d.id_tipo_denuncia;');

        dd($allinfo);

          return view('home');

        
    }*/
}
