<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Persona;
use App\User;
use Laracasts\Flash\Flash;

class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $personas = Persona::orderBy('id','ASC')->paginate(5);
        $personas->each(function($personas){
            $personas->users;
        });
        //dd("test");
        return view('admin.personas.index')->with('personas',$personas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('name','ASC')->lists('name','name');
        return view('admin.personas.createPersona')->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $personas = new Persona($request->all());
        //dd($persona)
        $personas->save();
        //FALTA ASIGNAR USUARIO A PERSONA
        return redirect()->route('admin.personas.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $persona = Persona::find($id);
        //dd($persona);
        return view('admin.personas.editPersona')->with('persona', $persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $persona = Persona::find($id);
        $persona->rut = $request->rut;
        $persona->nombre = $request->nombre;
        $persona->apellido = $request->apellido;
        $persona->correo = $request->correo;
        $persona->telefono = $request->telefono;
        $persona->save();

        return redirect()->route('admin.personas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $persona = User::find($id);
        //dd($persona);
        $persona->delete();

        //Flash::warning('La persona'. $persona->name . 'ha sido borrado de forma exitosa!');
        return redirect()->route('admin.users.index');
    }
}
