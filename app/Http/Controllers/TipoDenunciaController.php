<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tipo_Denuncia;
use App\Http\Requests\TipoDenunciaRequest;
//use vendor\laravel\framework\src\Illuminate\Support\Str.php;

class TipoDenunciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('te paseo index');
        $tipos_denuncias = Tipo_Denuncia::orderBy('id','ASC')->paginate(5);
        //como pasar datos de una variable a una vista
        return view('admin.tipos_denuncias.index')->with('tipos_denuncias',$tipos_denuncias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd('funcando o ke ?');
        return view('admin.tipos_denuncias.createTipoDenuncia');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoDenunciaRequest $request)
    {
        $tipos_denuncias = new Tipo_Denuncia($request->all());

        $tipos_denuncias->save();

        return redirect()->route('admin.tipos_denuncias.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_denuncia = Tipo_Denuncia::find($id);
        //dd($tipo_denuncia);
        return view('admin.tipos_denuncias.editTipoDenuncia')->with('tipo_denuncia',$tipo_denuncia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipo_denuncia = Tipo_Denuncia::find($id);
        $tipo_denuncia->tipo = $request->tipo;
        $tipo_denuncia->save();

        return redirect()->route('admin.tipos_denuncias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_denuncia = Tipo_denuncia::find($id);
        $tipo_denuncia->delete();

        return redirect()->route('admin.tipos_denuncias.index');
    }
}
