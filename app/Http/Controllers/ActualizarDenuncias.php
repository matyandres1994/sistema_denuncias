<?php

namespace App\Http\Controllers;

use App\Actualizar;
use Illuminate\Http\Request;
use App\Denuncia;
use App\User;

class ActualizarDenuncias extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.actualizar.actualizar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actualizaciones = Actualizar::all();
        return view('admin.actualizaciones.actualizar', ['actualizaciones' => $actualizaciones]);
        //return view('admin.actualizar.actualizar') ->with('estados',$estados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('subir_archivo'));
        $actualizarDenuncias                         = new Actualizar($request->all());
        $actualizarDenuncias->fecha_de_actualizacion = $request->fecha_actualizacion;
        $name                                        = $request->file('subir_archivo')->getClientOriginalName();
        $request->file('subir_archivo')->move('../uploads', $name);
        $actualizarDenuncias->subir_archivo = $name;
        $actualizarDenuncias->estado        = $request->tipo_estado;
        $actualizarDenuncias->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
