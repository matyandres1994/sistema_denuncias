<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Freshwork\ChileanBundle\Rut;

use ValidateRequests;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * aqui redirecciona despues de logearse
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * 
     * crea una nueva autenticacion de controlador de instanca
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * 
     * valida la registracion previamente al registro
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'rut' => 'required|cl_rut|unique:users',
            'name' => 'required|max:15',
            'apellido' => 'required|min:2',
            'email' => 'required|email|max:255|unique:users',
            'telefono' => 'required|min:6',
            'password' => 'required|confirmed|min:6',

            //'tipo_usuario' => 'required',
            //'terms' => 'required',
        ]);
    }

    /**
     * 
     * crea un nuevo usuario despues de validar el registro
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'rut' => $data['rut'],
            'name' => $data['name'],
            'apellido' => $data['apellido'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'password' => bcrypt($data['password']),
            //'tipo_usuario' => $data['tipo_usuario'],
        ]);
    }
}
