<?php

/*
|--------------------------------------------------------------------------
| Rutas de la aplicacion
|--------------------------------------------------------------------------
| Aqui es donde se pueden registrar todas las rutas de la aplicacion
| Simplemente dile a Laravel las URIs a los cuales deberia responder
| y darle al controlador para llamar cuando una URI es solicitada
| 
|
*/
/*
	GET,POST,PUT,DELETE,RESOURCE
*/	

Route::get('/', function () {
    return view('welcome');
    //auth.login
});
//Route::group(['prefix' => 'admin','middleware' => 'auth'],function(){

Route::group(['prefix' => 'admin'],function(){

	//Route::group(['middleware' => 'admin1'], function(){

		/*Route::resource('users','UsersController');
		Route::get('users/{id}/destroy', [
		'uses'	=>	'UsersController@destroy',
		'as'	=>	'admin.users.destroy'
		
		]);*/
	//});
	Route::resource('users','UsersController');
	
	Route::get('users/{id}/destroy', ['uses'	=>	'UsersController@destroy','as'	=>	'admin.users.destroy']);
	
	/*Route::resource('personas', 'PersonasController');
	Route::get('personas/{id}/destroy',['uses' => 'PersonasController@destroy','as' => 'admin.personas.destroy']);*/

	// Asignar personas a las denuncias
	Route::resource('user_denuncia','AsignacionesController');

	//Rutas de denuncia
	Route::resource('denuncias', 'DenunciasController');
	
	Route::get('denuncias/{id}/editEstadoDenuncia',['uses' => 'DenunciasController@editEstadoDenuncia','as' => 'admin.denuncias.editEstadoDenuncia']);

	Route::put('denuncias/{id}/updateEstadoDenuncia',['uses' => 'DenunciasController@updateEstadoDenuncia','as' => 'admin.denuncias.updateEstadoDenuncia']);

	Route::get('denuncias/{id}/destroy',['uses' => 'DenunciasController@destroy','as' => 'admin.denuncias.destroy']);


	Route::resource('archivos_denuncias', 'ArchivosDenunciasController');



	//Route::get('denuncias/mostrarDato',['uses' => 'DenunciasController@mostrarDato','as' => 'admin.denuncias.mostrarDato']);

	//Ruta estados
	Route::resource('estados', 'EstadosController');
	Route::get('estados/{id}/destroy',['uses' => 'EstadosController@destroy','as' => 'admin.estados.destroy']);

	Route::resource('tipos_denuncias', 'TipoDenunciaController');
	Route::get('tipos_denuncias/{id}/destroy',['uses' => 'TipoDenunciaController@destroy','as' =>
	'admin.tipos_denuncias.destroy']);

	Route::resource('roles', 'RolesController');
	Route::get('roles/{id}/destroy',['uses' => 'RolesController@destroy','as' =>
	'admin.roles.destroy']);

});

/*Route::get('auth/login', [
	'uses'	=>	'Auth\AuthController@getLogin',
	'as' => 'auth.login'

]);
Route::get('auth/login', [
	'uses'	=>	'Auth\AuthController@postLogin',
	'as' => 'auth.login'

]);*/

Route::get('auth/logout', [
	'uses'	=>	'Auth\AuthController@getLogout',
	'as' => 'auth.logout'

]);


//--------------------------------------------------
Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/home/f', 'DenunciasController@mostrarDato')->name('denuncia.mostrarDato');



Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/admin', 'HomeController@index');
 
});
