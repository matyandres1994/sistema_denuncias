<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    
    protected $table = "users";
    protected $fillable = ['rut','name','apellido', 'email', 'telefono','password'];

    
    protected $hidden = ['password', 'remember_token',];

    /*public function admin1(){

        return $this->tipo_usuario === 'admin1';

    }*/

    public function roles(){

        return $this->belongsToMany('App\Rol')->withTimestamps();
    }

     //es denuncias , yo lo cambie por denuncia
    public function denuncia(){
        return $this->hasMany('App\Denuncia');
    }
    

    public function denuncias(){

        return $this->belongsToMany('App\Denuncia');
    } 
}
