<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "estados";

    protected $fillable = ['tipo_estado'];

    public function denuncias(){

    	return $this->hasMany('App\Denuncia');

    }
}
