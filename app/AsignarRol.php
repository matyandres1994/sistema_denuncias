<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignarRol extends Model
{
    protected $table = "user_rol";

    protected $fillable = ['fecha_inicio','fecha_fin','user_id','rol_id'];
}
