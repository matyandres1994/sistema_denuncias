<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoDenuncia extends Model
{
    protected $table = "archivos_denuncias";

    protected $fillable = ['fecha_de_actualizacion','subir_archivo'];

    //public function denuncias(){

    //	return $this->hasMany('App\Denuncia');

    //}
}
