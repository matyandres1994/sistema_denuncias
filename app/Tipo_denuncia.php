<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_denuncia extends Model
{
    protected $table = "tipos_denuncias";

    protected $fillable = ['tipo'];

    public function denuncias(){

    	return $this->hasMany('App\Denuncia');
    }
}
