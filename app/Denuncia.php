<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    protected $table = "denuncias";

    protected $fillable = ['nombre_denuncia','fecha_agresion','pregunta1','pregunta2','pregunta3','subir_archivo','tipo_ayuda','id_tipo_denuncia','id_estado','id_user'/*,'id_archivo_denuncia'*/];

    public function tipo_denuncia(){

    	return $this->belongsTo('App\Tipo_denuncia');
    }

    public function estado(){

    	return $this->belongsTo('App\Estado');
    }

    /*public function archivos_denuncias(){

        return $this->belongsTo('App\ArchivoDenuncia');
    }*/

    
    public function user(){

        return $this->belongsTo('App\User');
    }
    


    public function users(){

    	return $this->belongsToMany('App\User');
    }
}
