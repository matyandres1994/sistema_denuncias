<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignar extends Model
{
    protected $table = "user_denuncia";

    protected $fillable = ['user_id','denuncia_id'];

    //public function denuncias(){

    //	return $this->belongsToMany('App\Denuncia');
    //}
}
