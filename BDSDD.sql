-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-01-2019 a las 07:24:46
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `taller_desarrollo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos_denuncias`
--

CREATE TABLE `archivos_denuncias` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_de_actualizacion` date NOT NULL,
  `subir_archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `denuncias`
--

CREATE TABLE `denuncias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_denuncia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_agresion` date NOT NULL,
  `pregunta1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pregunta2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pregunta3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subir_archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_ayuda` enum('psicologico','medico','asesor legal','no solicitar') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no solicitar',
  `id_tipo_denuncia` int(10) UNSIGNED NOT NULL,
  `id_estado` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `denuncias`
--

INSERT INTO `denuncias` (`id`, `nombre_denuncia`, `fecha_agresion`, `pregunta1`, `pregunta2`, `pregunta3`, `subir_archivo`, `tipo_ayuda`, `id_tipo_denuncia`, `id_estado`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'denuncia prueba', '2019-01-10', 'universidad', 'no', 'test', 'prisioner_right.png', 'psicologico', 1, 1, 1, '2019-01-10 09:21:32', '2019-01-10 09:21:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `tipo_estado`, `created_at`, `updated_at`) VALUES
(1, 'solicitado', '2019-01-10 08:59:34', '2019-01-10 08:59:34'),
(2, 'aceptado', '2019-01-10 09:00:27', '2019-01-10 09:00:27'),
(3, 'rechazado', '2019-01-10 09:00:32', '2019-01-10 09:00:32'),
(4, 'proceso', '2019-01-10 09:00:36', '2019-01-10 09:00:36'),
(5, 'baja', '2019-01-10 09:00:40', '2019-01-10 09:00:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_11_27_005032_create_roles_table', 1),
('2018_11_27_162834_create_tipos_denuncias_table', 1),
('2018_11_28_060929_create_estados_table', 1),
('2018_11_29_145706_create_denuncias_table', 1),
('2018_11_30_051321_create_archivos_denuncias', 1),
('2018_11_31_073232_create_user_denuncia_table', 1),
('2018_12_10_073207_create_user_rol_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `tipo_rol`, `created_at`, `updated_at`) VALUES
(1, 'Denunciante', '2019-01-10 09:04:30', '2019-01-10 09:04:30'),
(2, 'Director', '2019-01-10 09:04:36', '2019-01-10 09:04:36'),
(3, 'Investigador', '2019-01-10 09:04:43', '2019-01-10 09:04:43'),
(4, 'Psicologo', '2019-01-10 09:05:10', '2019-01-10 09:05:10'),
(5, 'Medico', '2019-01-10 09:05:15', '2019-01-10 09:05:15'),
(6, 'Abogado', '2019-01-10 09:05:19', '2019-01-10 09:05:19'),
(7, 'Administrador', '2019-01-10 09:05:24', '2019-01-10 09:05:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_denuncias`
--

CREATE TABLE `tipos_denuncias` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos_denuncias`
--

INSERT INTO `tipos_denuncias` (`id`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Laboral', '2019-01-10 09:01:59', '2019-01-10 09:01:59'),
(2, 'Sexual', '2019-01-10 09:02:05', '2019-01-10 09:02:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `rut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `rut`, `name`, `apellido`, `email`, `password`, `telefono`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '11.111.111-1', 'ADMINISTRADOR', 'admin', 'testing@testing.cl', '$2y$10$1B9VvU0HsIQrEQjVM3SUZeyRMacu7BnLMCO9TOhct/VcoYH53vWmK', '12345678', NULL, '2019-01-10 08:59:16', '2019-01-10 08:59:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_denuncia`
--

CREATE TABLE `user_denuncia` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `denuncia_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_rol`
--

CREATE TABLE `user_rol` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rol_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos_denuncias`
--
ALTER TABLE `archivos_denuncias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `denuncias`
--
ALTER TABLE `denuncias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `denuncias_id_tipo_denuncia_foreign` (`id_tipo_denuncia`),
  ADD KEY `denuncias_id_estado_foreign` (`id_estado`),
  ADD KEY `denuncias_id_user_foreign` (`id_user`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_denuncias`
--
ALTER TABLE `tipos_denuncias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_rut_unique` (`rut`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `user_denuncia`
--
ALTER TABLE `user_denuncia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_denuncia_user_id_foreign` (`user_id`),
  ADD KEY `user_denuncia_denuncia_id_foreign` (`denuncia_id`);

--
-- Indices de la tabla `user_rol`
--
ALTER TABLE `user_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_rol_user_id_foreign` (`user_id`),
  ADD KEY `user_rol_rol_id_foreign` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos_denuncias`
--
ALTER TABLE `archivos_denuncias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `denuncias`
--
ALTER TABLE `denuncias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipos_denuncias`
--
ALTER TABLE `tipos_denuncias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `user_denuncia`
--
ALTER TABLE `user_denuncia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_rol`
--
ALTER TABLE `user_rol`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `denuncias`
--
ALTER TABLE `denuncias`
  ADD CONSTRAINT `denuncias_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `denuncias_id_tipo_denuncia_foreign` FOREIGN KEY (`id_tipo_denuncia`) REFERENCES `tipos_denuncias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `denuncias_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_denuncia`
--
ALTER TABLE `user_denuncia`
  ADD CONSTRAINT `user_denuncia_denuncia_id_foreign` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_denuncia_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_rol`
--
ALTER TABLE `user_rol`
  ADD CONSTRAINT `user_rol_rol_id_foreign` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_rol_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
