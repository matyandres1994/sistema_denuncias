<!DOCTYPE html>
@section('htmlheader_title')
	Editar persona
@endsection

<!--	-->

<html lang="en">
<link href="{{ asset('/css/skins/skin-green.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />


@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
        	<!-- busca errores-->
        	@if(count($errors) > 0)
        		<div class="alert alert-danger" role="alert">
        			<ul>
        			@foreach($errors->all() as $error)
        				<li>{{ $error }}</li>
        			@endforeach
        			</ul>
        		</div>	
			@endif
			<!-- hasta aca -->
			<!-- Aqui se arma el formulario -->
            {!! Form::open(['route' => ['admin.tipos_denuncias.update',$tipo_denuncia], 'method' => 'PUT']) !!}


				<div class="form-group">
					{!! Form::label('tipo', 'Tipo Denuncia') !!}
					{!! Form::text('tipo',$tipo_denuncia->tipo,['class' => 'form-control','placeholder' => 'ingrese tipo denuncia','required']) !!}
				</div>

				
				<div class="form-group">
					{!! Form::submit('Editar',['class' => 'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>

</html>
