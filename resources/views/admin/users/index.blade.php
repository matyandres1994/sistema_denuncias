<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />
<title> SDD - @yield('htmlheader_title', 'Usuarios') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
        	
        	<div class="panel panel-info class ">
        		
  				<div class="panel-heading">Instrucciones </div>
  			 	<div class="panel-body">• </div>
  			</div>
  			 	@if( $users->count() < 1 )
				    <div class="panel panel-danger ">
        		
  						<div class="panel-heading">Atencion! </div>
  			 			<div class="panel-body">• No hay usuarios 	</div>
  			 		</div>
				@endif
        	<ul class="nav navbar-nav">
            <li><a href="{{ route('admin.users.create') }}"class="btn btn-info">Ingresar Usuario</a><hr></li>
            </ul>
            
        	<table id="users"  class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Rut</th>
					<th>Rol</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Email</th>
					<th>telefono</th>
					<th></th>
					<th>Editar / Eliminar</th>
				</thead> 
				<tbody>
					
					
					@foreach($users as $user)
					
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->rut }}</td>
			
							<td></td>
							
							<td>{{ $user->name }}</td>
							<td>{{ $user->apellido }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->telefono }}</td>
							
							<td>
								
									<!--<span class="label label-danger">{{ $user->tipo_usuario }}</span>
								
									<span class="label label-primary">{{ $user->tipo_usuario }}</span>-->
								
							</td>
							<td><a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

							<a href="{{ route('admin.users.destroy', $user->id) }}" onclick="return confirm('¿Seguro que deseas eliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><a/></td>					
							
						</tr>		
					@endforeach
					
				</tbody>
			</table>
			<!--table class="panel panel-default">

				<thead >

					<th class="panel-body">ID</th>

					<th class="panel-body">Roles</th>
				</thead>
				<tbody>
					@foreach($users as $rol_user)
						<tr>
							<td class="panel-body"> {{ $rol_user->id }}</td>
							<td>
                                <a >
                                    {{ $rol_user->rol_id }}
                                </a>
                            </td>
						</tr>
					@endforeach
				</tbody>
			</table-->
			<div class="text-center">
				{!! $users->render() !!}
			</div>	
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
