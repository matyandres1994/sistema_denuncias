<!DOCTYPE html>
@section('htmlheader_title')
    Editar usuario
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">

            <!-- busca errores-->
            @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>  
            @endif
            
            {!! Form::open(['route' => ['admin.users.update',$user], 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                    {!! Form::text('name',$user->name,['class' => 'form-control','placeholder' => 'nombre usuario','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('apellido', 'Apellido') !!}
                    {!! Form::text('apellido',$user->apellido,['class' => 'form-control','placeholder' => 'Apellido usuario','required']) !!}
                </div>

                <!-- agregar un combo box------------------------------------------>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email',$user->email,['class' => 'form-control','placeholder' => 'Ingrese el email','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('telefono', 'Telefono') !!}
                    {!! Form::text('telefono',$user->telefono,['class' => 'form-control','placeholder' => 'Telefono usuario','required']) !!}
                </div>





                <!--<div class="form-group">
                    {!! Form::label('tipo_usuario','tipo') !!}
                    {!! Form::select('tipo_usuario',['admin2' => 'Administrador 2','admin1' => 'Administrador 1'],$user->tipo_usuario,['class' =>'form-control']) !!}
                </div>-->



                <div class="form-group">
                    {!! Form::submit('Editar',['class' => 'btn btn-primary']) !!}
                </div>

            {!! Form::close() !!}
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
