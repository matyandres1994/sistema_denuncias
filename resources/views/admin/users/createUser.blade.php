<!DOCTYPE html>
@section('htmlheader_title')
	Crear Usuario
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
    
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
        <!-- busca errores-->
        	@if(count($errors) > 0)
        		<div class="alert alert-danger" role="alert">
        			<ul>
        			@foreach($errors->all() as $error)
        				<li>{{ $error }}</li>
        			@endforeach
        			</ul>
        		</div>	
			@endif
		<!-- hasta aca -->
        <!-- Main content -->
        <div class="panel panel-info class ">
                
                <div class="panel-heading">Instrucciones </div>
                <div class="panel-body">
                • Complete los campos dados y luego presione el boton "Registrar"
                <div></div>
                • Si desea volver a la pagina anterior, presione "Cancelar" 
                </div>
            </div>
        <section class="input-group-addon">


        	<p class="login-box-msg">{{ trans('Registre un nuevo usuario') }}</p>
            {!! Form::open(['route' => 'admin.users.store', 'method' => 'POST']) !!}

                <div class="form-group  col-xs-push-2 col-sm-7">
                    
    				<div class="form-group has-feedback">
<<<<<<< HEAD
                        <input type="cl_rut" class="form-control" placeholder="{{ trans('rut') }}" name="rut" onchange="" value="{{ old('rut') }}"/>
=======
                        <input type="cl_rut" class="form-control" placeholder="{{ trans('rut') }}" name="rut" value="{{ old('rut') }}"/>
>>>>>>> 48d0ade3a5cfeb281867d15ab2cfef86afc2828a
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="name" class="form-control"  placeholder="{{ trans('nombres') }}" name="name" value="{{ old('name') }}"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <!--div class="input-group">
                        <input type="text" class="form-control" aria-label="...">
                        <div class="input-group-btn">
                                
                        </div>
                    </div-->

                    <div class="form-group has-feedback" >
                        <input type="apellido" class="form-control" placeholder="{{ trans('apellidos') }}" name="apellido" value="{{ old('apellido') }}"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="{{ trans('email') }}" name="email" value="{{ old('email') }}"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="telefono" class="form-control" placeholder="{{ trans('telefono') }}" name="telefono" value="{{ old('telefono') }}"/>
                        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                    </div>
                    
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="{{ trans('Contraseña') }}" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="{{ trans('Reescribir contraseña') }}" name="password_confirmation"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <!--accion del tipo de usuario -->
                    <!--<div class="form-group has-feedback">

                        {!! Form::label('tipo_usuario','tipo') !!}
                        {!! Form::select('tipo_usuario',[''=> 'seleccione un nivel..','admin2' => 'Administrador 2','admin1' => 'Administrador 1'],null) !!}
                        
                    </div>-->
                    
                    <div class="form-group">
                        {!! Form::submit('Registrar',['class' => 'btn btn-primary']) !!}

                        <a href="{{ route('admin.users.index') }}"class="btn btn-danger mb-3"> Cancelar</a>
                    </div>
                </div>
			{!! Form::close() !!}
			
            @yield('main-content')

        </section><!-- /.content -->
        
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
    @section('js')
            @section('js')
            <script>
                function validaRut(Objeto) {
                    var tmpstr = "";
                    var intlargo = Objeto.value
                    if (intlargo.length > 0) {
                        crut = Objeto.value
                        largo = crut.length;
                        if (largo < 2) {
                            alert('rut inválido')
                            Objeto.focus()
                            return false;
                        }
                        for (i = 0; i < crut.length; i++)
                            if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
                                tmpstr = tmpstr + crut.charAt(i);
                            }
                        rut = tmpstr;
                        crut = tmpstr;
                        largo = crut.length;
                        if (largo > 2) rut = crut.substring(0, largo - 1);
                        else rut = crut.charAt(0);
                        dv = crut.charAt(largo - 1);
                        if (rut == null || dv == null) return 0;
                        var dvr = '0';
                        suma = 0;
                        mul = 2;
                        for (i = rut.length - 1; i >= 0; i--) {
                            suma = suma + rut.charAt(i) * mul;
                            if (mul == 7) mul = 2;
                            else mul++;
                        }
                        res = suma % 11;
                        if (res == 1) dvr = 'k';
                        else if (res == 0) dvr = '0';
                        else {
                            dvi = 11 - res;
                            dvr = dvi + "";
                        }
                        if (dvr != dv.toLowerCase()) {
                            alert('El rut ingresado es invalido');
                            // Objeto.style.backgroundColor = '#F18C8C';
                            Objeto.value = "";
                            Objeto.focus();
                            return false;
                        }
                        //Objeto.style.backgroundColor = '#6FD84C';
                        Objeto.focus()
                        return true;
                    }
                }
            </script>


        @endsection
@show

</body>
</html>
