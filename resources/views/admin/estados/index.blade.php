<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<title> SDD - @yield('htmlheader_title', 'Estados') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content col col-lg-8">
            
            <ul class="nav navbar-nav">
            <li><a href="{{ route('admin.estados.create') }}"class="btn btn-info " >Ingresar estado </a> <hr></li>
            </ul>


        	<table class="table table-striped ">
				<thead>
					<th></th>
					<th>ID</th>
					<td></td>
					<th></th>
					<th>Tipos de estado</th>
					<th></th>
					<th></th>
					<td></td>
					<th>Editar / Eliminar</th>
				</thead> 
				<tbody>
					@foreach($estados as $estado)
						<tr>
							<th></th>
							<th>{{ $estado->id }}</th>
							<td></td>
							<td></td>
							<td>{{ $estado->tipo_estado }}</td>	
							<th></th>
							<th></th>
							<th></th>

							<td><a href="{{ route('admin.estados.edit', $estado->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

							<a href="{{ route('admin.estados.destroy', $estado->id) }}" onclick="return confirm('¿Seguro que deseas eliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><a/></td>					
							
						</tr>		
					@endforeach
				</tbody>
			</table>
			<div class="text-center">
				{!! $estados->render() !!}
			</div>	
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
