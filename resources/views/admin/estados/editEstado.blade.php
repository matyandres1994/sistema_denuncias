<!DOCTYPE html>
@section('htmlheader_title')
    Editar Estado Denuncia
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="input-group-addon">

            <!-- busca errores-->
            @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>  
            @endif
            
            {!! Form::open(['route' => ['admin.estados.update',$estado], 'method' => 'PUT']) !!}

                <div class="input-group col-xs-push-4">
                    {!! Form::label('tipo_estado', 'Eliga el tipo de estado') !!}

                    {!! Form::select('tipo_estado',['solicitado' => 'Solicitado','aceptado' => 'Aceptado','rechazado' => 'Rechazado','proceso' => 'Proceso','baja' => 'Baja'],null,['class' =>'form-control','placeholder' => 'ingrese estado','required']) !!}
                    
                    <div class="input-group col-xs-6 col-sm-11" >
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                    
                    <a href="{{ route('admin.estados.index') }}"class="btn btn-danger"> Cancelar</a>

                </div>

                </div>

                

                

            {!! Form::close() !!}
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
