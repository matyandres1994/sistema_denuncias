<!DOCTYPE html>
@section('htmlheader_title')
	Crear Roles
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
        <!-- busca errores-->
        	@if(count($errors) > 0)
        		<div class="alert alert-danger" role="alert">
        			<ul>
        			@foreach($errors->all() as $error)
        				<li>{{ $error }}</li>
        			@endforeach
        			</ul>
        		</div>	
			@endif
		<!-- hasta aca -->
        <!-- Main content -->
        <section class="content">
            {!! Form::open(['route' => 'admin.roles.store', 'method' => 'POST']) !!}
				

				<div class="form-group">

					{!! Form::label('tipo_rol', 'Ingrese un tipo de rol') !!}

					{!! Form::text('tipo_rol',null,['class' => 'form-control','placeholder' => 'Ej:Director','required']) !!}

				</div>
				
				


				<div class="form-group">
					{!! Form::submit('Registrar',['class' => 'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}
			
            @yield('main-content')

        </section><!-- /.content -->
        @section('js')
	        <!--<script>
	        	$('.select-vehiculo').chosen({})
	        	$('.select-vehiculo').chosen();
	        	$('.textarea-content').trumbowyg();
	        </script>-->


        @endsection
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
