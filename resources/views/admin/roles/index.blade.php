<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<title> SDD - @yield('htmlheader_title', 'Roles') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            @if( $roles->count() < 1 )
				<div class="panel panel-danger ">
        		
  					<div class="panel-heading">Atencion! </div>
  			 		<div class="panel-body">• No hay roles ingresados,Para ingresar debe hacer click en "ingresar tipo rol" 	</div>
  			 	</div>
			@endif
            <ul class="nav navbar-nav">
            <li><a href="{{ route('admin.roles.create') }}"class="btn btn-info">Ingresar tipo rol</a><hr></li>
            </ul>

        	<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Tipos de Rol</th>
					<th></th>
					<th></th>
					<th>Editar / Eliminar</th>
				</thead> 
				<tbody>
					@foreach($roles as $rol)
						<tr>
							<td>{{ $rol->id }}</td>
							<td>{{ $rol->tipo_rol }}</td>
							
							<td></td><td></td>
							
							<td><a href="{{ route('admin.roles.edit', $rol->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

							<a href="{{ route('admin.roles.destroy', $rol->id) }}" onclick="return confirm('¿Seguro que deseas eliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><a/></td>					
							
						</tr>		
					@endforeach
				</tbody>
			</table>
			<div class="text-center">
				{!! $roles->render() !!}
			</div>	
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
