<!DOCTYPE html>
@section('htmlheader_title')
	Crear denuncia
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
<title> SDD - @yield('htmlheader_title', 'Ingresar denuncia') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
        <!-- busca errores-->
        	@if(count($errors) > 0)
        		<div class="alert alert-danger" role="alert">
        			<ul>
        			@foreach($errors->all() as $error)
        				<li>{{ $error }}</li>
        			@endforeach
        			</ul>
        		</div>	
			@endif
			@if (session('status')) 
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif
		<!-- hasta aca -->
        <!-- Main content -->
        <section class="input-group-addon">
            {!! Form::open(['route' => 'admin.denuncias.store', 'method' => 'POST','enctype' => 'multipart/form-data']) !!}


				<div class="form-group  col-xs-push-2 col-sm-7">
					

					<div class="form-group has-feedback">
						{!! Form::label('nombre_denuncia', 'Nombre denuncia') !!}
	                    <input type="text" class="form-control" placeholder="{{ trans('Nombre denuncia') }}" name="nombre_denuncia" value="{{ old('nombre_denuncia') }}" required autofocus/>
	                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
	                </div>

	                
					<div class="form-group has-feedback">
						{!! Form::label('fecha_agresion', 'Fecha de la agresion') !!}

	                    <input type="date" class="form-control" placeholder="{{ trans('Fecha agresion') }}" name="fecha_agresion" value="{{ old('nombre_denuncia') }}" required autofocus/>
	                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
	                </div>
					
					
					<div class="form-group">
						{!! Form::label('pregunta1', '¿En que lugar ocurrieron los hechos?') !!}
						{!! Form::text('pregunta1',null,['class' => 'form-control','placeholder' => 'Ingrese respuesta...','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('pregunta2', '¿Conoce al agresor?, si es asi indique nombre y vinculo con la universidad') !!}
						{!! Form::text('pregunta2',null,['class' => 'form-control','placeholder' => 'Ingrese respuesta','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('pregunta3', 'Explique lo ocurrido') !!}
						{!! Form::text('pregunta3',null,['class' => 'form-control','placeholder' => 'Ingrese respuesta','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('subir_archivo', 'Subir archivo de pruebas') !!}
						{!! Form::file('subir_archivo',null,['class' => 'form-control','placeholder' => 'aprendiendo a subir archivos eoeo','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('tipo_ayuda', 'Eliga el tipo de ayuda') !!}

						{!! Form::select('tipo_ayuda',['psicologico' => 'Ayuda psicologica','medico' => 'Ayuda medica','asesor legal' => 'Ayuda legal','no solicitar' => 'No solicitar'],null,['class' => 'form-control select-tag','placeholder' => 'seleccione ayuda a solicitar','required']) !!}
					</div>
					<!--FALTA AGREGAR TIPO DENUNCIA, ESTADO Y user-->

					<div class="form-group">
						{!! Form::label('id_tipo_denuncia_', 'Tipo denuncia') !!}
						{!! Form::select('id_tipo_denuncia', $tipos_denuncias, null,['class' => 'form-control','placeholder' => 'seleccione un tipo','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('id_estado', 'Estado') !!}
						{!! Form::select('id_estado', $estados, null,['class' => 'form-control','placeholder' => 'seleccione un estado','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('id_user', 'Usuario') !!}
						{!! Form::select('id_user', $users, null,['class' => 'form-control','placeholder' => 'seleccione un usuario','required']) !!}
					</div>


						<div class="form-group">
							{!! Form::submit('Registrar',['class' => 'btn btn-primary']) !!}

							<a href="{{ route('admin.denuncias.index') }}"class="btn btn-danger mb-3"> Cancelar</a>
						</div>

				</div>
			{!! Form::close() !!}
			
            @yield('main-content')
        </section><!-- /.content -->
        @section('js')
	        <script>
	        	$('.select-tag').chosen({

	        	});
	        </script>


        @endsection
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
    
@show

</body>
</html>
