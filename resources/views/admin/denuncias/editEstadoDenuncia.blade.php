<!DOCTYPE html>
@section('htmlheader_title')
    Editar Estado denuncia
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />
<title> SDD - @yield('htmlheader_title', 'Editar estado denuncia') </title>

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">

            <!-- busca errores-->
            @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>  
            @endif
            <section class="input-group-addon">
	            {!! Form::open(['route' => ['admin.denuncias.updateEstadoDenuncia',$denuncia], 'method' => 'PUT']) !!}
	            <div class="form-group  col-xs-push-2 col-sm-7">
					<div class="form-group">
						{!! Form::label('id_estado', 'Estado') !!}
						{!! Form::select('id_estado', $estados, $denuncia->id_estado,['class' => 'form-control default->proceso','required']) !!}
					</div>

					<div class="form-group">
	                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
	                    {!! Form::reset('Restablecer valores',['class' => 'btn btn-warning']) !!}

	                    <a href="{{ route('admin.denuncias.index') }}"class="btn btn-danger"> Cancelar</a>
	                </div>
	            </div>
	            {!! Form::close() !!}
	        </section>
	        <hr>

	        <section class="row col-lg-4">
	        	<div class="panel panel-info ">
        			<div class="panel-heading ">Instrucciones </div>
  			 		<div class="panel-body ">• Para ingresar una denuncia debe clickear "ingresar denuncia"	</div>
				</div>
				<div class="panel panel-info ">
        			<div class="panel-heading ">Instrucciones </div>
  			 		<div class="panel-body ">• Para ingresar una denuncia debe clickear "ingresar denuncia"	</div>
				</div>

	        </section>





            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
