<!DOCTYPE html>
@section('htmlheader_title')
    Editar Denuncia
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">

            <!-- busca errores-->
            @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>  
            @endif
            
            {!! Form::open(['route' => ['admin.denuncias.update',$denuncia], 'method' => 'PUT']) !!}

                <div class="form-group">
					{!! Form::label('nombre_denuncia', 'Nombre denuncia') !!}

					{!! Form::text('nombre_denuncia',$denuncia->nombre_denuncia,['class' => 'form-control','placeholder' => 'Nombre denuncia..','required']) !!}
				</div>


				<div class="form-group">
					{!! Form::label('fecha_agresion', 'Fecha Agresion') !!}

					{!! Form::date('fecha_agresion',$denuncia->fecha_agresion,['class' => 'form-control','placeholder' => 'Ingrese fecha','required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('pregunta1', '¿En que lugar ocurrieron los hechos?') !!}
					{!! Form::text('pregunta1',$denuncia->pregunta1,['class' => 'form-control','placeholder' => 'Ingrese respuesta...','required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('pregunta2', '¿Conoce al agresor?, si es asi indique nombre y vinculo con la universidad') !!}
					{!! Form::text('pregunta2',$denuncia->pregunta2,['class' => 'form-control','placeholder' => 'Ingrese respuesta','required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('pregunta3', 'Explique lo ocurrido') !!}
					{!! Form::text('pregunta3',$denuncia->pregunta3,['class' => 'form-control','placeholder' => 'Ingrese respuesta','required']) !!}
				</div>

				<!-- Implementar la parte del seba -->
				<!--<div class="form-group">
					{!! Form::label('subir_archivo', 'Subir archivo de pruebas') !!}
					{!! Form::file('subir_archivo',null,['class' => 'form-control','placeholder' => 'aprendiendo a subir archivos ']) !!}
				</div>-->

				<!-- hasta aca es subir archivos -->
				<div class="form-group">
					{!! Form::label('tipo_ayuda', 'Eliga el tipo de ayuda') !!}

					{!! Form::select('tipo_ayuda',['psicologico' => 'Ayuda psicologica','medico' => 'Ayuda medica','asesor legal' => 'Ayuda legal','no solicitar' => 'No solicitar'],$denuncia->tipo_ayuda,['class' =>'form-control select-tag']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('id_tipo_denuncia', 'Tipo denuncia') !!}

					{!! Form::select('id_tipo_denuncia', $tipos_denuncias,$denuncia->id_tipo_denuncia,['class' => 'form-control select-tipo_denuncia','required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('id_estado', 'Estado') !!}
					{!! Form::select('id_estado', $estados, $denuncia->id_estado,['class' => 'form-control default->proceso','required']) !!}
				</div>

				



                <div class="form-group">
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                    {!! Form::reset('Restablecer valores',['class' => 'btn btn-warning']) !!}

                    <a href="{{ route('admin.denuncias.index') }}"class="btn btn-danger"> Cancelar</a>
                </div>
                

                


            {!! Form::close() !!}
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
