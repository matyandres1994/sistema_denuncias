<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<title> SDD - @yield('htmlheader_title', 'Denuncias') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

    	@if(Auth::user())
        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">

        	<div class="panel panel-danger ">
        		
  				<div class="panel-heading">Instrucciones </div>
  			 	<div class="panel-body">• Para ingresar una denuncia debe clickear "ingresar denuncia"	</div>
			</div>

        	<ul class="nav navbar-nav">
            <li>
            	<a href="{{ route('admin.denuncias.create') }}"class="btn btn-info">Ingresar Denuncias</a>
            	<hr>
            </li>
            </ul>


            
        	<table  class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Nombre denuncia</th>
					<th>Fecha Agresión</th>
					<th>Pregunta 1</th>
					<th>Pregunta 2</th>
					<th>Pregunta 3</th>
					
					<th>Ayuda solicitada</th>
					<th>Tipo denuncia</th>
					<th>Estado denuncia</th>
					<th>Rut Denunciante</th>
					<th>Editar / Eliminar</th>
				</thead> 
				<tbody>
					@foreach($denuncias as $denuncia)
						<tr>
						

							<td>{{ $denuncia->id }}</td>
							<td>{{ $denuncia->nombre_denuncia }}</td>
							<td>{{ $denuncia->fecha_agresion }}</td>
							<td>{{ $denuncia->pregunta1 }}</td>
							<td>{{ $denuncia->pregunta2 }}</td>
							<td>{{ $denuncia->pregunta3 }}</td>
                          
							<td>{{ $denuncia->tipo_ayuda }}</td>

							<td>{{ \App\Tipo_denuncia::find($denuncia->id_tipo_denuncia)->tipo}}</td>
							<td>
								<a href="{{route('admin.denuncias.editEstadoDenuncia', $denuncia->id)}}">

									{{ \App\Estado::find($denuncia->id_estado)->tipo_estado}}
								</a>
							</td>
							<td>{{ \App\User::find($denuncia->id_user)->rut}}</td>

							
							
							

							<td>
								<a href="{{ route('admin.denuncias.edit', $denuncia->id) }}" 
									class="btn btn-warning" >

									<span 
										class="glyphicon glyphicon-wrench" aria-hidden="true">
									</span>
								</a>

								<a href="{{ route('admin.denuncias.destroy', $denuncia->id) }}" 
									onclick="return confirm('¿Seguro que deseas eliminarlo?')" 
									class="btn btn-danger">

									<span 
										class="glyphicon glyphicon-remove-circle" aria-hidden="true">
									</span>
								<a/>
							</td>					
							
						</tr>
						<!--div class="pull-right"		
							<i class="fa fa-clock-o"></i> {{ $denuncia->created_at }}
						</div-->
						
					@endforeach
					

				</tbody>
			</table>
			<table class="panel panel-default">

				<thead >

					<th class="panel-body">ID</th>

					<th class="panel-body">Archivos</th>
				</thead>
				<tbody>
					@foreach($denuncias as $archivos_denuncia)
						<tr>
							<td class="panel-body"> {{ $archivos_denuncia->id }}</td>
							<td>
                                <a download="{{ $archivos_denuncia->subir_archivo }}"
                                	href="../../uploads/{{$archivos_denuncia->subir_archivo}}">
                                    {{ $archivos_denuncia->subir_archivo }}
                                </a>
                            </td>
						</tr>
					@endforeach
				</tbody>
			</table>
			

			<div class="text-center">
				{!! $denuncias->render() !!}
			</div>	
            @yield('main-content')
        </section><!-- /.content -->
        @endif
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
