<!DOCTYPE html>
@section('htmlheader_title')
    Actualizar denuncias
@endsection
<html lang="en">
    <link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}">
    </script>
    @section('htmlheader')
    @include('layouts.partials.htmlheader')
@show
    <!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
    <body class="skin-purple" sidebar-collapse="">
        <div class="wrapper">
            @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @include('layouts.partials.contentheader')
                <!-- busca errores-->
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!-- hasta aca -->
                <!-- Main content -->
                <section class="content">
                    {!! Form::open(['route' => 'admin.actualizar.store', 'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                        {!! Form::label('fecha_actualizacion', 'Fecha de actualizacion') !!}
                    {!! Form::date('fecha_actualizacion',null,['class' => 'form-control','placeholder' => 'Ingrese fechas','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('subir_archivo', 'Subir archivo de pruebas') !!}
                    {!! Form::file('subir_archivo',null,['class' => 'form-control','placeholder' => 'aprendiendo a subir archivos >:-x','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tipo_estado', 'Eliga el tipo de estado') !!}

                    {!! Form::select('tipo_estado',['solicitado' => 'Solicitado','Proceso' => 'Proceso','baja' => 'Baja'],null,['class' =>'form-control']) !!}
                    </div>
                    <!--FALTA AGREGAR TIPO DENUNCIA, ESTADO Y RUT PERSONA-->
                    <div class="form-group">
                        {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    archivos
                                </th>
                                <th>
                                    fecha de actualizacion
                                </th>
                                <th>
                                    Estado
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($archivos as $hola)
                            <tr>
                                <td>
                                    <?php echo $hola->
                                    id; ?>
                                </td>
                                <td>
                                    <a download="<?php echo $hola->subir_archivo ?>" href="../../../uploads/<?php echo $hola->subir_archivo ?>">
                                        <?php echo $hola->
                                        subir_archivo ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo $hola->
                                    fecha_de_actualizacion ?>
                                </td>
                                <td>
                                    <?php echo $hola->
                                    estado ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @yield('main-content')
                </section>
                <!-- /.content -->
                @section('js')
                <!--<script>
                $('.select-vehiculo').chosen({})
                $('.select-vehiculo').chosen();
                $('.textarea-content').trumbowyg();
            </script>-->
                @endsection
            </div>
            <!-- /.content-wrapper -->
            @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')
        </div>
        <!-- ./wrapper -->
        @section('scripts')
    @include('layouts.partials.scripts')
@show
    </body>
</html>
