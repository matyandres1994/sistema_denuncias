		<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<title> SDD - @yield('htmlheader_title', 'Asignaciones') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--

|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">

        	<ul class="nav navbar-nav">
            <li><a href="{{ route('admin.user_denuncia.create') }}"class="btn btn-info"> Asignar persona</a><hr></li>
            </ul>
            
        	<table  class="table table-striped">
				<thead>
					<th>ID</th>
					<th></th>
					<th>Fecha asignacion</th>
					<th>Rut persona</th>
					<th>Rol persona</th>
					<th>Denuncia asociada</th>
					<th>Estado Denuncia</th>
					<th>Editar / Eliminar</th>
				</thead> 
				<tbody>
					@foreach($user_denuncia as $asignacion)
						<tr>
						

							<td>{{ $asignacion->id }}</td>
							<td></td>
							<td> {{ $asignacion->created_at }}</td>
							<td>
                                <a 
                                	href="{{ url('admin/users')}}">
                                    {{ \App\User::find($asignacion->user_id)->rut }}
                                </a>
                            </td>
							
							<td></td>

							<td>
								<a
									href="{{url('admin/denuncias')}}">
									{{ \App\Denuncia::find($asignacion->user_id)->nombre_denuncia }}
								</a>
							</td>

							
							<td>{{ \App\Estado::find($asignacion->user_id)->tipo_estado }}</td>	
										
						</tr>
					@endforeach
					

				</tbody>
			</table>
			
			<div class="text-center">
				{!! $user_denuncia->render() !!}
			</div>	
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
