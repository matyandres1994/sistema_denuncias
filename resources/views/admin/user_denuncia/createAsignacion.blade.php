<!DOCTYPE html>
@section('htmlheader_title')
	Asignar
@endsection

<html lang="en">

<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
<title> SDD - @yield('htmlheader_title', 'Asignar') </title>
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-purple" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')
        <!-- busca errores-->
        	@if(count($errors) > 0)
        		<div class="alert alert-danger" role="alert">
        			<ul>
        			@foreach($errors->all() as $error)
        				<li>{{ $error }}</li>
        			@endforeach
        			</ul>
        		</div>	
			@endif
		<!-- hasta aca -->
        <!-- Main content -->
        <section class="input-group-addon col-sm-16">
            {!! Form::open(['route' => 'admin.user_denuncia.store', 'method' => 'POST']) !!}
				

				<div class="form-group">

					{!! Form::label('user_id', 'Usuario') !!}

					{!! Form::select('user_id', $users, null,['class' => 'form-control','placeholder' => 'seleccione un usuario','required']) !!}
				</div>

				<div class="form-group">

					{!! Form::label('denuncia_id', 'Denuncia') !!}

					{!! Form::select('denuncia_id', $denuncias, null,['class' => 'form-control','placeholder' => 'seleccione una denuncia','required']) !!}
				</div>
				
				


				<div class="form-group col-xs-push--4 col-sm-3">
                    <div class="form-group" >
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                    
                    <a href="{{ route('admin.user_denuncia.index') }}"class="btn btn-danger"> Cancelar</a>
				</div>

			{!! Form::close() !!}
			
            @yield('main-content')

        </section><!-- /.content -->
        @section('js')
	        <!--<script>
	        	$('.select-vehiculo').chosen({})
	        	$('.select-vehiculo').chosen();
	        	$('.textarea-content').trumbowyg();
	        </script>-->


        @endsection
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
