@if(Auth::user())
@extends('layouts.app')

@section('htmlheader_title')
	Inicio
@endsection


@section('main-content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Inicio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link rel="stylesheet" href="{{ asset('plugins/chosen/chosen.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.css') }}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body >


	<div class="container" style="width:100%" background=>
	  
	  <div id="myCarousel" class="carousel slide" data-ride="carousel" >
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">

				<div class="item active" >
					<img src="{{ asset('/img/fondo.png') }}" alt="" style="width:100%;">
					    <div class="carousel-caption">
					        <h3>Aqui va algun texto</h3>
					        <p></p>
					    </div>
				</div>

				<!--<div class="item ">
					<img src="{{ asset('/img/asd.jpg') }}" alt="" style="width:100%;">
					    <div class="carousel-caption">
					        <h3>CONTROL DE NEUMATICOS</h3>
					        <p></p>
					    </div>
				</div>  -->
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">

				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>

			</a>
	  </div>
	</div>
	
</body>
</html>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script>

@yield('js')
<script>
    $('.carousel').carousel({
        interval: 4500
    })
</script>
</body>
</html>
@endsection
@endif

