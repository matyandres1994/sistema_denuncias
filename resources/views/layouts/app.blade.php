<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-yellow.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-red.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-black.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-purple.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-blue-light.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/skins/skin-black-light.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('plugins/chosen/chosen.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.css') }}">
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue" sidebar-collapse>
<div class="wrapper">

    @include('layouts.partials.mainheader')
    @include('flash::message')
    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content" >
            <!-- Your Page Content Here -->

            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    @include('flash::message')
    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
