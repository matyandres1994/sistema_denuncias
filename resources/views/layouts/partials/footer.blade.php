<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <!--a href="https://github.com/acacha/adminlte-laravel"></a><b>Neumaticos Pako</b></a>. {{ trans('adminlte_lang::message.descriptionpackage') }}-->
    </div>
    <!-- Default to the left -->
    <strong> </a><b>Sistema de denuncias</b></a> Copyright &copy; 2018  <a href="http://jpgit.ubb.cl/simios"></a>.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="http://jpgit.ubb.cl/simios">Simios</a>. <!--{{ trans('adminlte_lang::message.seecode') }} <a href="https://github.com/acacha/adminlte-laravel">Github</a-->
</footer>