<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        {{ Auth::user()->name }}
                    </p>
                    <!-- Status -->
                    <a href="#">
                        <i class="fa fa-circle text-success">
                        </i> 
                        {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <!--form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form-->
        <!-- /.search form -->
        <style >
            .active{
                background: #999
            }
        </style>
        <!-- Sidebar Menu -->
        @if(Auth::user())
        <ul class="sidebar-menu">
            
            <li class="header">
                {{ trans('MODULOS') }}
            </li>
            <!-- Optionally, you can add icons to the links -->


                <li class="treeview {{ request()->is('/') ? 'active' : '' }}">
                    <a href="{{ url('home') }}">
                        <i class='glyphicon glyphicon-home '>
                            
                        </i> 
                        <span>
                            {{ trans('Inicio') }}
                        </span>
                    </a>
                </li>

                <!-- USUARIOS -->
                <!-- VALIDACION DE ROLES -->
                
                <li class="treeview">
                    <a href="">
                        <i class='glyphicon glyphicon-user'>
                        </i> 
                        <span>
                            {{ trans('Usuarios') }}
                        </span> 
                        <i class="fa fa-angle-left pull-right">
                        </i>
                    </a>

                    <ul class="treeview-menu">
                    <li>
                        <a href="{{ url('admin/users')}}">
                            {{ trans('Lista de usuarios') }}
                        </a>
                    </li>
                </ul>

                </li>
                
                
                
                <!-- DENUNCIAS -->
                <li class="treeview" >
                    <a href="#">
                        <i class='glyphicon glyphicon-list'>
                        </i>
                        <span>
                            {{ trans('Denuncias') }}
                        </span> 
                        <i class="fa fa-angle-left pull-right">
                        </i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/denuncias')}}">
                                <i class='glyphicon glyphicon-list-alt'>
                                </i>
                                {{ trans('Lista de denuncias') }}
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ url('admin/denuncias/create')}}">
                                <i class='glyphicon glyphicon-hand-right'>
                                </i>
                                {{ trans('ingresar denuncia') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/archivos_denuncias/create') }}">
                                {{ trans('Actualizar denuncias')}}
                            </a>
                        </li>
                    </ul>
                </li>

                
                <!-- ROLES -->
                <li class="treeview">
                    <a href="#"><i class='glyphicon glyphicon-eye-open'></i> <span>{{ trans('Roles') }}</span> <i class="fa fa-angle-left pull-right"></i></a>

                    <ul class="treeview-menu">

                        <li><a href="{{ url('admin/roles')}}"><i class='glyphicon glyphicon-list-alt'></i>{{ trans('Lista de roles') }}</a></li>
                        
                        <li><a href="{{ url('admin/roles/create')}}"><i class='glyphicon glyphicon-hand-right'></i>{{ trans('ingresar rol') }}</a></li>
                        
                    </ul>
                </li>

                

                <!-- DERIVACIONES -->
                
                <li class="treeview" >
                    <a href="#">
                        <i class='glyphicon glyphicon-hand-right'>
                        </i>
                        <span>
                            {{ trans('Asignaciones') }}
                        </span> 
                        <i class="fa fa-angle-left pull-right">
                        </i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/user_denuncia')}}">
                                <i class='glyphicon glyphicon-list-alt'>
                                </i>
                                {{ trans('Lista asignaciones') }}
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ url('admin/user_denuncia/create')}}">
                                <i class='glyphicon glyphicon-hand-right'>
                                </i>
                                {{ trans('Asignar personas') }}
                            </a>
                        </li>
                    </ul>
                </li>


                <!--TIPOS DENUNCIA-->

                <li class="treeview">
                    <a href="{{ url('admin/tipos_denuncias') }}">
                        <i class='glyphicon glyphicon-indent-left'></i> 

                        <span>
                            {{ trans('Tipos denuncia') }}
                        </span> 
                    </a>

                </li>
        

                <!-- ESTADO DENUNCIA-->
                <li class="treeview">
                    <a href="{{ url('admin/estados') }}">
                        <i class='glyphicon glyphicon-check'></i> 

                        <span>
                            {{ trans('Estados denuncia') }}
                        </span> 
                    </a>
                </li>
            <!-- fin comentario -->
        </ul><!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>
