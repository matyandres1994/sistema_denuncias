@extends('layouts.mainForMaintainers')

@section('styleHead-M')
    <style type="text/css">
        @media (min-width : 998px) {
            .div-date-calendar{
                border-right: solid 2px #d2d2d2;
                margin-top: 10px;
            }
        }

        @media (max-width : 998px) {
            .logo2{
                display: none;
            }
            .div-calendar-full-size{
                /*display: none;*/
            }

            .div-title-calendar{
                margin-top: -10px !important;
                text-align: left;
                padding-top: 15px;
                margin-left: 15px;
            }

            .name-day-calendar{
                display: block;
            }

            .div-responsive-info-calendar{
                margin-left: 100px;
                margin-top: -65px !important;
                border-left: 2px solid #d2d2d2;
            }

            .info-responsive-calendar{
                margin-top: -40px;
                margin-left: 40px;
            }
        }
        .transparent-style{
            background-color: rgba(255, 255, 255, .4)!important;
        }
        .post-sticky {
            border: 2px solid goldenrod;
            color: #666;
            -webkit-box-shadow: 7px 4px 10px 0px rgba(50, 50, 50, 0.7);
            -moz-box-shadow:    7px 4px 10px 0px rgba(50, 50, 50, 0.7);
            box-shadow:         7px 4px 10px 0px rgba(50, 50, 50, 0.7);
        }

        button.loading {
            background: url({{url()->to('/')}}/dist/img/loader.gif) no-repeat;
            background-size: 25px 25px;
            background-position:right center;
        }
        /*.img-zentin {
            width: 50px;
            height: 50px;
        }
        .img-zentin-reaction {
            width: 60px;
            height: 50px;
        }*/
        .into-event-px{
            display: table;
            margin-top: 7px;
            width: 0;
            height: 100px;
            font-size: 2.0em;
            position: absolute;
            right: 185px;
            top: 45px;
        }

        .balloon{
            position: relative;
            text-decoration: none;
        }
        .balloon span {display: none;}
        .balloon:hover span {
            display: block;
            position: absolute;
            padding: .5em;
            content: attr(title);
            text-align: center;
            width: auto;
            height: auto;
            white-space: nowrap;
            top: -40px;
            background: rgba(0,0,0,.8);
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            color: #fff;
            font-size: 0.86em;
            font-family: Arial, Helvetica, sans-serif;
        }
        .balloon:hover span:after {
            position: absolute;
            display: block;
            content: "";
            border-color: rgba(0,0,0,.8) transparent transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            bottom: -20px;
            left: 1em;
        }
        .info-carousel{
            z-index: 2;
            margin-bottom: -50px;
            position: relative !important;
            background-color: rgba(0, 0, 0, 0.4);
            color: #fff;
        }

        .calendar-icon{
            z-index: 1;
            position: absolute;;
        }

        .div-title-calendar{
            z-index: 2;
            position: relative;
        }

        .box-min-height-galeria-logros{
            min-height: 330px;
        }

        .profile-user-img-NC{
            border: 3px solid #ef5350 !important
        }

        .box-photo-NC{
            width: 80px;
            height: 80px;
            background: white;
            display: inline;
            margin-left: -15px;
        }

        .description-TDM-new{
            margin-left: -20px;
            text-align: justify;
        }

        .morecontent span {
            display: none;
        }

        .zoom-in {
            transition: -webkit-transform 0.25s ease;
            cursor: zoom-in;
        }

        .img-post-active {
            -webkit-transform: scale(1.30);
            z-index: 99999999;
            position: absolute;
            cursor: zoom-out;

        }

        zoom-in:hover {opacity: 0.7;}

        .img-pop {
            cursor: pointer;
            margin: 5px auto;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }

        .img-pop-modal {
            margin-top: 2px;
        }
        .star-TDM {
            width: 35px;
            height: 35px;
            position: absolute;
            top: 147px !important;
            left: 41px !important;
        }

        .jqstooltip {
            z-index: 10000000000 !important;
        }
        /*YOUTUBE VIDEO RESPONSIVE*/
        .youtube-responsive iframe,
        .youtube-responsive object,
        .youtube-responsive embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .hashtag-post{
            cursor: pointer;
        }

        .new-textarea-post{
            padding-top: 10px;
            padding-bottom: 80px;
            max-height: 80px;
            overflow-y: auto;
            text-align: left;
        }

        /*.popover{
            width: 100% !important;
        }*/

    </style>

    @yield('styleHead')
@endsection

