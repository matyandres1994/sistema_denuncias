@extends('layouts.auth')

@section('htmlheader_title')
    Registro cuenta
@endsection

@section('content')

<body background="{{ asset('/img/FONDO_LOGIN2.jpg') }}">
    <div class="register-box">
        

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ups!</strong> {{ trans('Ha ocurrido un problema al rellenar los datos') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="register-box-body">
            <div class="register-logo">
                <a href="{{ url('/home') }}"><b>Sistema</b>de denuncias</a>
            </div>
            <p class="login-box-msg">{{ trans('Registre un nuevo usuario') }}</p>
            <form action="{{ url('/register') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group has-feedback">
                    <input type="rut" class="form-control" placeholder="{{ trans('rut') }}" name="rut" value="{{ old('rut') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="name" class="form-control" placeholder="{{ trans('nombres') }}" name="name" value="{{ old('name') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="apellido" class="form-control" placeholder="{{ trans('apellidos') }}" name="apellido" value="{{ old('apellido') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="{{ trans('email') }}" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="telefono" class="form-control" placeholder="{{ trans('telefono') }}" name="telefono" value="{{ old('telefono') }}"/>
                    <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="{{ trans('Contraseña') }}" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="{{ trans('Reescribir contraseña') }}" name="password_confirmation"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <!--accion del tipo de usuario -->
                <!--<div class="form-group has-feedback">

                    {!! Form::label('tipo_usuario','tipo') !!}
                    {!! Form::select('tipo_usuario',[''=> 'seleccione un nivel..','admin2' => 'Administrador 2','admin1' => 'Administrador 1'],null) !!}
                    
                </div>-->
                
                <div class="row">
                    <div class="col-xs-10 col-xs-push-1">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('Registrar') }}</button>
                    </div><!-- /.col -->
                </div>
            </form>



            <h4><a href="{{ url('/login') }}" class="text-center">{{ trans('Actualmente ya tengo una cuenta') }}</a></h4>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    @include('layouts.partials.scripts_auth')


    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
