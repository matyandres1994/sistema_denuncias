@extends('layouts.auth')

@section('htmlheader_title')
    Log in auth
@endsection

@section('content')

<body background="{{ asset('/img/FONDO_LOGIN2.jpg') }}">
 
    <div class="login-box" >
        
        
         
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ups !</strong> {{ trans('Ha ocurrido un problema al rellenar los datos') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>Sistema</b>de denuncias</a>
            </div><!-- /.login-logo -->
            
            <p class="login-box-msg"> {{ trans('Iniciar sesión') }} </p>
            <form action="{{ url('/login') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="{{ trans('Contraseña') }}" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-4">
                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('Ingresar') }}</button>
                    </div><!-- /.col -->
                </div>
            </form>



            <a href="{{ url('/password/reset') }}">
                {{ trans('He olvidado mi contraseña') }}
            </a>
            <br>
            <a href="{{ url('/register') }}" class="text-center">
                {{ trans('Registrar nuevo usuario') }}
            </a>
        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

        @include('layouts.partials.scripts_auth')


</body>

@endsection
