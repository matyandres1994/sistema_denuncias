@extends('layouts.app')

@section('htmlheader_title')
    {{ trans('Pagina no disponible') }}
@endsection

@section('contentheader_title')
    {{ trans('404 error') }}
@endsection

@section('$contentheader_description')
@endsection

@section('main-content')

<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! {{ trans('la pagina que ingreso no se encuentra disponible') }}.</h3>
        <p>
            {{ trans('No pudimos encontrar la pagina que andas buscando') }}
            {{ trans('podrias volver a la' ) }} <a href='{{ url('/home') }}'>{{ trans('pagina principal') }}</a>
        </p>
        
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection