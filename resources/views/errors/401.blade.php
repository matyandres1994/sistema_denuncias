@extends('layouts.app')

@section('htmlheader_title')
    Usuario restringido
@endsection



@section('$contentheader_description')
@endsection

@section('main-content')

    <div class="error-page">
        <h5 class="headline text-red">Acceso Restringido</h5>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Solo los usuarios con los permisos pueden acceder a esta seccion {{ trans('') }}</h3>
            <p>
                
                {{ trans('Puede volver a la') }} <a href='{{ url('/home') }}'>{{ trans('pagina inicio') }}</a> 
            </p>
            
        </div>
    </div><!-- /.error-page -->
@endsection